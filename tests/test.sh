#!/bin/sh -e
# Lint
echo "Linting role..."
yamllint ../ || true
# Syntax check
echo "Checking role syntax..."
ansible-galaxy install -r requirements.yml
ansible-playbook --syntax-check all.yml
# Ansible Playbook
echo "Running role..."
ansible-playbook -vv all.yml
