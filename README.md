Docker
======
Installs and starts Docker and optionally configures Docker Swarm

Supported Operating Systems
---------------------------
* CentOS 7

Requirements
------------
No Requirements

Role Variables
--------------
```
---
firewall_docker_services:
  - name: swarm
    description: 'Docker Swarm'
    priority: 1
    zones: '{{ docker_firewall_zones }}'
    rules:
      - protocol: tcp
        port: 2377
      - protocol: tcp
        port: 7946
      - protocol: udp
        port: 7946
      - protocol: udp
        port: 4789
```

Role Defaults
-------------
```
---
swarm_leader: <bool> (if the node is the leader) false
swarm_manager: <bool> (if the node is a non-leader manager) false
swarm_worker: <bool> (if the node is a worker) false
docker_firewall_zones: <list> firewall zones to be created
  - '{{ firewall_default_zone }}'
```

Dependencies
------------
* firewall

Configuring a swarm
-------------------
Swarms can be configured by setting `swarm_leader: true` on the primary master, `swarm_manager: true` on other masters, and `swarm_worker: true` on workers.

Additionally, the following variables need to be set:

```
---
swarm_advertise_addr: <string> The address to advertise on the leader
swarm_leader_host: The Ansible hostname (that needs to be DNS resolvable) of the leader
```

Tasks must be run on the leader first to get the relevant enrollment tokens. Make note of this when structuring your inventory files.

Testing
-------
Tests can be found in the `tests` directory. Current tests validate the following:

* yamllint
* Syntax check
* Provisioning

### Local Testing
Local testing can be done by running the `docker-compose.yml` file found in the `tests` directory

### CI Testing
CI testing configuration can be found in `.gitlab-ci.yml` in the root directory

License
-------
ISC

Author Information
------------------
CV Admins <cvadmins@utdallas.edu>
